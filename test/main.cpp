#define CATCH_CONFIG_MAIN

#include <catch.hpp>

#include <microfiche/microfiche.hpp>
#include <microfiche/prelude.hpp>

namespace {
    class ephemeral_file {
        const path file_;

    public:
        ephemeral_file(const std::string& contents)
            : file_(file::temp_directory_path() / file::unique_path()) {
            if (!empty(contents)) {
                try {
                    auto out = fstream(file_, std::ios::out);
                    out << contents;
                } catch (...) {
                    if (exists(file_)) {
                        remove(file_);
                    }
                }
            }
        }

        ~ephemeral_file() {
            if (exists(file_)) {
                remove(file_);
            }
        }

        auto get() const -> path { return file_; }
        operator path() const { return file_; }
    };
}  // namespace

namespace print {
    struct proc {
        int indent_level = 0;
        int n_indent_calls = 0;
        int n_dedent_calls = 0;
        int n_process_file_calls = 0;
        int n_empty_lines = 0;
    };

    auto process(proc& p, int row, int column, string_view line) -> void {
        p.n_process_file_calls++;
        if (empty(line)) p.n_empty_lines++;
        cout << '(' << row << ", " << column << "): " << line << endl;
    }

    auto indent(proc& p, int old_column, int new_column) -> void {
        const auto n_spaces = new_column - old_column;
        p.indent_level++;
        p.n_indent_calls++;
        cout << "INDENT: " << p.indent_level << endl;
    }

    auto dedent(proc& p, int old_column, int new_column) -> void {
        const auto n_spaces = old_column - new_column;
        p.indent_level--;
        p.n_dedent_calls++;
        cout << "DEDENT: " << p.indent_level << endl;
    }
}

TEST_CASE("Indent/dedent hooks called", "[ws-tree]") {
    // clang-format off
    auto f = ephemeral_file(R"(
foo.txt:
    This is a file.
)");
    // clang-format on

    auto proc = print::proc();

    mf::process_file(proc, f);

    REQUIRE(proc.indent_level == 0);
    REQUIRE(proc.n_indent_calls == 1);
    REQUIRE(proc.n_dedent_calls == 1);
    REQUIRE(proc.n_process_file_calls == 3);
}

TEST_CASE("Nested indentation", "[ws-tree]") {
    // clang-format off
    auto f = ephemeral_file(R"(
foo.txt:
    This is a file.
        This is nested.
)");
    // clang-format on

    auto proc = print::proc();

    mf::process_file(proc, f);

    REQUIRE(proc.indent_level == 0);
    REQUIRE(proc.n_indent_calls == 2);
    REQUIRE(proc.n_dedent_calls == 2);
    REQUIRE(proc.n_process_file_calls == 4);
}

TEST_CASE("Blank lines allowed", "[ws-tree]") {
    // clang-format off
    auto f = ephemeral_file(R"(
A
    B
    
    C
)");
    // clang-format on

    auto proc = print::proc();

    mf::process_file(proc, f);

    REQUIRE(proc.indent_level == 0);
    REQUIRE(proc.n_indent_calls == 1);
    REQUIRE(proc.n_dedent_calls == 1);
    REQUIRE(proc.n_process_file_calls == 5);
}
