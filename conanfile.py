import os
from conans import ConanFile, CMake

class ALKConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = (
        "boost/1.67.0@conan/stable",
        "range-v3/0.3.5@ericniebler/stable",
        "Catch/1.12.1@bincrafters/stable ",
        "variant/1.3.0@bincrafters/stable",
        "optional-lite/2.3.0@bincrafters/stable ",
    )
    generators = "cmake", "virtualrunenv"
    build_policy = "missing" 

    def build(self):
        cmake = CMake(self, generator='Ninja')
        cmake.configure()
        cmake.build()
        cmake.test()
