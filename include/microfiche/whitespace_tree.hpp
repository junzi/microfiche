#pragma once

#include <boost/hof.hpp>
#include <range/v3/getlines.hpp>
#include <range/v3/view/drop.hpp>
#include <range/v3/view/take_while.hpp>
#include <range/v3/view/transform.hpp>

#include <microfiche/prelude.hpp>

namespace mf {
    using std::isblank;

    BOOST_HOF_STATIC_LAMBDA_FUNCTION(lines_with_location) = boost::hof::pipable([](auto&& is) {
        return view::zip(view::indices, getlines(is)) | view::transform([](auto&& p) {
                   auto&& [row, line] = p;
                   const auto column =
                       find_if(line, [](auto c) { return !isblank(c); }) - begin(line);
                   return make_tuple(row, column,
                                     string_view(data(line) + column, size(line) - column));
               });
    });

    template <typename Processor>
    auto indent(Processor&& proc, int old_column, int new_column) -> void {}

    template <typename Processor>
    auto dedent(Processor&& proc, int old_column, int new_column) -> void {}

    template <typename Processor>
    auto process(Processor&& proc, int row, int column, string_view line) -> void {}

    template <class Processor, class Stream>
    auto process(Processor&& proc, Stream& is, int initial_margin = 0) -> void {
        auto margins = vector<int>{initial_margin};

        for (auto [row, column, line] : is | lines_with_location) {
            const auto margin = back(margins);

            if (column > margin) {
                indent(proc, margin, column);
                margins.push_back(column);
            } else {
                while (column < back(margins)) {
                    dedent(proc, margin, column);
                    margins.pop_back();
                }
            }

            process(proc, row, column, line);
        }

        while (size(margins) > 1) {
            const auto old_column = margins.back();
            margins.pop_back();
            const auto new_column = margins.back();
            dedent(proc, old_column, new_column);
        }
    }

    template <class Processor>
    auto process_file(Processor&& proc, const file::path& f) {
        auto is = file::ifstream(f);
        process(forward<Processor>(proc), is);
    }
}

