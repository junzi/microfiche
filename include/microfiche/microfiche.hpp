#pragma once

#include <microfiche/prelude.hpp>
#include <microfiche/whitespace_tree.hpp>

namespace mf {
    using std::ostream_iterator;
    using std::runtime_error;

    namespace details {
        struct state {
            file::path dir;
            file::path file;
            int n_spaces = -1;

            optional<file::ofstream> out;

            state(const file::path& dir) : dir(dir) {}
        };

        auto process(state& s, int row, int column, string_view line) -> void {
            using namespace std::literals;

            if (s.out)
                *s.out << string(column - s.n_spaces, ' ') << line << '\n';
            else {
                auto& dir = s.dir;

                if (empty(line)) return;

                auto c = back(line);
                if (c != ':' && c != '/') throw runtime_error("Unable to parse: "s + string(line));

                line.remove_suffix(1);

                s.file = s.dir / string(line);

                if (c == ':') {
                    s.out.emplace(s.file);
                    s.n_spaces = -1;
                } else {
                    s.n_spaces = column;
                }
            }
        }

        auto indent(state& s, int old_column, int new_column) -> void {
            if (!s.out) {
                s.dir = s.file;
                file::create_directories(s.dir);
            } else if (s.n_spaces == -1) {
                s.n_spaces = new_column;
            }
        }

        auto dedent(state& s, int old_column, int new_column) -> void {
            if (!s.out)
                s.dir = s.dir.parent_path();
            else {
                if (new_column <= s.n_spaces) {
                    s.out.reset();
                    s.n_spaces = -1;
                }
            }
        }
    }

    auto expand(const file::path& in, const file::path& out) -> void {
        auto is = file::ifstream(in);
        auto state = details::state(out);

        file::create_directories(out);

        process(state, is);
    }

    auto compress(const file::path& in, const file::path& out, int indent_spaces = 4) -> void {
        if (file::exists(out)) file::remove(out);
        auto os = std::ofstream(file::change_extension(out, "mf").string().c_str(),
                                std::ios::out | std::ios::trunc);

        auto b = file::recursive_directory_iterator(in);
        auto e = file::recursive_directory_iterator();

        auto dir = out;

        for (auto i = b; i != e; ++i) {
            const auto f = i->path();
            auto column = indent_spaces * i.level();

            os << string(column, ' ') << f.filename().string();

            if (file::is_regular_file(f)) {
                os << ":\n";
                const auto spaces = string(column + indent_spaces, ' ');
                auto is = file::ifstream(f);
                copy(getlines(is) | view::transform([=](auto&& line) -> string {
                         return spaces + string(line);
                     }),
                     ranges::ostream_iterator<string>(os, "\n"));
            } else if (file::is_directory(f)) {
                os << "/\n";
            }
        }
    }
}
