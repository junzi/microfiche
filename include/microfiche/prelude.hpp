#pragma once

#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/hana.hpp>
#include <mpark/variant.hpp>
#include <range/v3/action/push_back.hpp>
#include <range/v3/action/sort.hpp>
#include <range/v3/action/unique.hpp>
#include <range/v3/algorithm/find.hpp>
#include <range/v3/algorithm/find_if.hpp>
#include <range/v3/algorithm/lower_bound.hpp>
#include <range/v3/algorithm/rotate.hpp>
#include <range/v3/algorithm/sort.hpp>
#include <range/v3/algorithm/unique.hpp>
#include <range/v3/algorithm/upper_bound.hpp>
#include <range/v3/core.hpp>
#include <range/v3/view/concat.hpp>
#include <range/v3/view/indices.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/single.hpp>
#include <range/v3/view/zip.hpp>

using std::cout;
using std::endl;
using std::forward;
using std::move;
using std::reference_wrapper;
using std::string;
using std::string_view;
using std::stringstream;
using std::vector;

namespace fs = boost::filesystem;
namespace hana = boost::hana;
namespace file = boost::filesystem;

using namespace ranges;

using fs::fstream;
using fs::ifstream;
using fs::ofstream;
using fs::path;

using mpark::variant;
