# Microfiche

Microfiche is text file format which uses python-style whitespace sensitivity in order
to amalgamate several text files into one. You can think of it like `tar`, but for humans.
Microfiche is useful for configuration and build scripts, which are typically small, many, and
varied. 

Here is an example of a simple *Microfiche* file:

```
foo.txt:
    This is a test file.
    It's written in microfiche.
bar.txt:
    This is another file. 
    Also written in microfiche.
some_dir/
    baf.txt:
        Wow, this is in a subdirectory!
    subdir/
        other.txt:
            Yet another file in a sub-subdirectory.
```


## Building Microfiche

Microfiche is built using CMake and conan. Instructions for installing and using conan can be found at:

https://docs.conan.io/en/latest/installation.html

The program was tested on a fresh digital ocean box with the following specs:

- 1 GB Memory 
- 25 GB Disk 
- Region: FRA1 
- Ubuntu 18.04.4 x64 

On Ubuntu, conan can be installed using:

    sudo apt update
    sudo apt install python-pip
    export LC_ALL='en_US.UTF-8'
    python -m pip install conan 

CMake is also required, and can be installed using:

    sudo apt install cmake

Once conan is installed, the following commands can be used to build `microfiche`:

    cd microfiche
    source dotmf
    conan install . --install-folder build --build missing 
    conan build . --build-folder build

The last command builds `microfiche` and runs the test suite, leaving the executable in 
`./build/bin`.
