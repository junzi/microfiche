// Microfiche.

#include <boost/program_options.hpp>

#include <microfiche/microfiche.hpp>

auto main(int argc, const char* argv[]) -> int {
    const auto in = file::path(argv[1]);
    const auto out = file::path(argv[2]);

    if (!file::exists(in)) return 1;

    if (file::is_regular_file(in))
        mf::expand(in, out);
    else
        mf::compress(in, out);

    return 0;
}
